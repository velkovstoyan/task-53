import "../scss/app.scss";

window.addEventListener("DOMContentLoaded", () => {
    let priceValue = document.querySelector('.price').textContent;
    let product = document.querySelector('.product')
    product.setAttribute('data-price', priceValue)
});
